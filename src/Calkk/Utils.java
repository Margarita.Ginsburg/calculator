package Calkk;

public class Utils {

    public Integer getIntValue(String as) {

        char[] node = new char[]{'1','2','3','4','5','6','7','8','9','0'};
        boolean check;

        for (char s : as.toCharArray()) {
            check = false;
            for (char n : node) {
                if (n == s) {
                    check = true;
                }
            }
            if (!check){
                return -1;
            }
        }

        return Integer.parseInt(as);
    }

    public Task getTask(String as){

        switch (as){
            case "+":
                return Task.PLUS;
            case "-":
                return Task.MINUS;
            case "*":
                return Task.MULTIPLY;
            case "/":
                return Task.DIVIDE;
            default:
                return null;

        }
    }
}
