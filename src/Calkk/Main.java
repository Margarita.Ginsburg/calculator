package Calkk;

import java.io.Console;

public class Main {

    public static void main(String[] args) {


        Console cnsl = System.console();
        Calkulyator calkulyator = new Calkulyator();

        if (cnsl == null) {
            System.out.println("No console available");
            return;
        }

        int a = -1;
        do {
            String str = cnsl.readLine("Enter string : ");
            a = calkulyator.checkNumber(str);
        } while (a == -1);

        int b = -1;
        do {
            String str = cnsl.readLine("Enter second string : ");
            b = calkulyator.checkNumber(str);
        } while (b == -1);

        Task sign;
        do {
            String str = cnsl.readLine("Enter mathematical action : ");
            sign = calkulyator.getTask(str);
        } while (sign == null);


        int out = 0;

            switch (sign){
                case PLUS:
                    out = a + b;
                    break;
                case MINUS:
                    out = a - b;
                    break;
                case DIVIDE:
                    out = a / b;
                    break;
                case MULTIPLY:
                    out = a * b;
                    break;
                default:
            }

        System.out.println(out);


        // Print
        System.out.println("You entered : " + out);
    }
}
